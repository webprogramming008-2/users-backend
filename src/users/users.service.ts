import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let uesrs: User[] = [
  { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234' },
  { id: 2, login: 'user1', name: 'User 1', password: 'Pass@1234' },
  { id: 3, login: 'user2', name: 'User 2', password: 'Pass@1234' },
];
let lastUserId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    const newUser: User = {
      id: lastUserId++,
      ...createUserDto, // login, name, password
    };

    uesrs.push(newUser);
    return newUser;
  }

  findAll() {
    return uesrs;
  }

  findOne(id: number) {
    const index = uesrs.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return uesrs[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = uesrs.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log(uesrs[index]);
    // console.log(updateUserDto);
    const updateUser: User = {
      ...uesrs[index],
      ...updateUserDto,
    };
    uesrs[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = uesrs.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = uesrs[index];
    uesrs.splice(index, 1);
    return deletedUser;
  }
  reset() {
    uesrs = [
      { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234' },
      { id: 2, login: 'user1', name: 'User 1', password: 'Pass@1234' },
      { id: 3, login: 'user2', name: 'User 2', password: 'Pass@1234' },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
